const { PN532 } = require('pn532')
const ndef = require('ndef')

const pollInterval = 500
const tagTimeoutMs = pollInterval * 1.5

class Reader extends PN532 {
  constructor (serialport) {
    super(serialport, {pollInterval})
    this.currentTag = null
    this.tagTimer
    this.on('ready', this.rfidReady)
  }

  rfidReady () {
    console.log('Reader ready')
    this.on('tag', this.newTag)
  }

  newTag (tag) {
    // Detect tag removal by timing out tag detection
    clearTimeout(this.tagTimer)
    this.tagTimer = setTimeout(() => {
      if (this.currentTag) {
        this.emit('off', this.currentTag)
        this.currentTag = null
      }
    }, tagTimeoutMs)

    // Prevent repeated parsing of the same tag
    if (this.currentTag && this.currentTag.uid == tag.uid) {
      return
    }
    this.currentTag = tag
    this.emit('on', tag)
  }
}

module.exports = Reader
