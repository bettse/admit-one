const { RTD_TEXT, RTD_URI, RTD_SMART_POSTER, TNF_WELL_KNOWN, TNF_EMPTY, decodeMessage } = require('ndef')
const SerialPort = require('serialport')
const mqtt = require('mqtt')
const Reader = require('./reader')

const {SERIAL_PORT, MQTT_URL, MQTT_USER, MQTT_PASS} = process.env
if (!SERIAL_PORT || !MQTT_URL || !MQTT_USER || !MQTT_PASS) {
  console.log('Must define SERIAL_PORT, MQTT_URL, MQTT_USER, MQTT_PASS')
  process.exit()
}

const uart = new SerialPort(SERIAL_PORT, { baudRate: 115200 })
const rfid = new Reader(uart)
const client = mqtt.connect(MQTT_URL, {username: MQTT_USER, password: MQTT_PASS})

function describeWKT (record) {
  switch (record.type) {
    case RTD_TEXT:
      record.recordType = 'RTD_TEXT'
      break
    case RTD_URI:
      record.recordType = 'RTD_URI'
      break
    case RTD_SMART_POSTER:
      record.recordType = 'RTD_SMART_POSTER'
      if (!record.value) {
        var records = decodeMessage(record.payload)
        record.value = describeRecords(records)
      }
      break
  }
  if (record.value) {
    delete record.payload
  }
  return record
}

function describeRecords (records) {
  return records.map((record) => {
    record.typeNameFormat = 'TNF_WELL_KNOWN'
    switch (record.tnf) {
      case TNF_WELL_KNOWN:
        return describeWKT(record)
      case TNF_EMPTY:
        break
      default:
        return record
    }
  })
}

function onMessage (topic, message) {
  // message is Buffer
  console.log('message', message.toString())
}

rfid.on('ready', async () => {
  const firmware = await rfid.getFirmwareVersion()
  console.log(`firmware: ${JSON.stringify(firmware, 0, 2)}`)

})

client.on('connect', () => {
  console.log('mqtt connected')
  client.on('message', onMessage)

  rfid.on('on', async (tag) => {
    console.log({tag})

    try {
      const ndefData = await rfid.readNdefData()
      const records = decodeMessage(Array.from(ndefData))
      tag.ndef = describeRecords(records)
    } catch (e) {
      // not ndef formatted
    }
    // Save ATQA as string for serialization
    tag.ATQA = tag.ATQA.readUInt16BE(0).toString(0x10).padStart(4, '0')

    client.publish(`on/${tag.uid}`, JSON.stringify(tag))
  })

  rfid.on('off', (tag) => {
    client.publish(`off/${tag.uid}`, JSON.stringify(tag))
  })
})

